
# PARÁMETROS 
FECHA_CORTE <- '2021-03-31'  # fecha más reciente cortar los datos, formato "AAAA-MM-DD"
FECHA_INSERCION <- format(Sys.Date(), "%Y-%m-%d") # fecha de carga de resultados a Oracle, formato "AAAA-MM-DD"
VERSION <- 'VERSION 6'      #  Número de Versión con la cuál se guarda en la tabla resultados en Oracle
GUARDAR_EN_ORACLE <- FALSE  # TRUE para guardar resultados en Oracle, FALSE para NO guardarlos

###############################
library(targets)

source('R/00_funciones_auxiliares.R', encoding = "UTF8")
source('R/a01_preprocesamiento.R', encoding = "UTF8")
source('R/a02_calcular_rfm.R', encoding = "UTF8")
source('R/a03_calcular_cdv.R', encoding = "UTF8")
source('R/a04_calcular_indicadores.R', encoding = "UTF8")
source('R/a05_consolidar_rfm_cdv_indicadores.R', encoding = "UTF8")
source('R/a06_segmentacion_aps.R', encoding = "UTF8")

options(tidyverse.quiet = TRUE)
options(clustermq.scheduler = "multiprocess")

tar_option_set(packages = c("ROracle",
                            "tidyverse",
                            "tidyr",
                            "dbplyr", 
                            "dplyr",
                            "lubridate",
                            "rfm"))

# Flujo

extraccion_y_limpieza_base <- 
  tar_target(A01_BASE_APS, preprocesamiento(FECHA_CORTE))

calculando_rfm <-
  tar_target(A02_RFM, loop_rfm_cvd_ind(A01_BASE_APS,
                                       'RFM',
                                       FECHA_INSERCION))

calculando_cdv <-
  tar_target(A03_CDV, loop_rfm_cvd_ind(A01_BASE_APS,
                                       'CDV'))

calculando_indicadores <- 
  tar_target(A04_INDICADORES, loop_rfm_cvd_ind(A01_BASE_APS,
                                               'Indicadores'))

consolidando_rfm_cdv_ind <-
  tar_target(A05_RFM_CDV_IND, 
             consolidar_rfm_cdv_indicadores(A02_RFM,
                                            A03_CDV,
                                            A04_INDICADORES))
segmentando_aps <- 
  tar_target(A06_SEGMENTACION_APS,
             segmentacion_aps(A05_RFM_CDV_IND,
                              VERSION))

guardar_cvd_rfm_seg <- 
  tar_target(A06_GUARDAR_ORACLE,
             guardar_en_oracle(tabla_R = A06_SEGMENTACION_APS,
                               esquema_Ora = "DWHREP",
                               tabla_Ora = "RES_CICLO_VIDA_SEG_APS_DET",
                               guardar = GUARDAR_EN_ORACLE))


list(extraccion_y_limpieza_base,
     calculando_rfm,
     calculando_cdv,
     calculando_indicadores,
     consolidando_rfm_cdv_ind,
     segmentando_aps,
     guardar_cvd_rfm_seg)

