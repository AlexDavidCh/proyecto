<img style="float: left;" src="informes/ads.png" width = '75px'/> 

## ETL APS ADS

<hr>

### Descripción

Este ETL sirve para calcular indicadores, métricas y scores de Asesores Productores de Seguros (APS), con información de ADS, los resultados se guardan en la siguiente tabla de la base de datos Oracle:

 + **DWHREP.RES_CICLO_VIDA_SEG_APS_DET**

El proceso completo está desarrollado mediante el paquete de R **targets** para el control del flujo de trabajo de inicio a fin.

### Ejecución

Para ejecutar el ETL,  previamentte se debe editar el archivo "_targets.R" y definir los 4 parámetros generales del ETL:

+ **FECHA_CORTE:** corresponde a la fecha más actual con la que se cortarán los datos, en formato "AAAA-MM-DD".
+ **FECHA_INSERCION:** corresponde a la fecha de carga de los resultados a la base de datos, en formato "AAAA-MM-DD". Por defecto se utiliza la fecha actual del servidor.
+ **VERSION:** corresponde al número de Versión con la cuál se guarda en la tabla resultados en Oracle
+ **GUARDAR EN ORACLE:** parámetro para guardar resultados en la tabla de oracle,  TRUE para guardar resultados en Oracle, FALSE para NO guardarlos

Una vez definidos los parámetros se deben ejecutar los 3 comandos del script  *ejecutar.R*:

```
library(targets)
tar_watch(seconds = 20, outdated = FALSE, targets_only = TRUE) # despliega el panel de control web (opcional)
tar_make() # ejecuta el proceso
```

